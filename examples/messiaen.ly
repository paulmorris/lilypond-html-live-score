\version "2.19.36"

\paper {
 
  page-breaking = #ly:one-line-auto-height-breaking
 
}

\header {
  title = "Quatuor pour la fin du temps"
  subtitle = "VI. Danse de la fureur, pour les sept trompettes"
  composer = "Olivier Messiaen"
  tagline = ##f
}

\score {
  
  

  \new Staff \with {
    instrumentName = "Réduction"
  }
  {
    
    \override Score.MetronomeMark.vertical-skylines = #'()
 
    \accidentalStyle neo-modern
    \autoBeamOff
 
    \relative fis' {
   
   
      \omit Score.TimeSignature
   
      \tempo "Décidé, vigoureux, granitique, un peu vif" 8 = 176
   
      \set Score.markFormatter = #format-mark-box-alphabet
   
      \mark \default
 
      \time 17/16
      fis8[ e] bes'[(-- c16 aes8]) fis[ e] bes'4		|
      fis8[ e] bes'16[( b8) bes16 c] fis,2		|
      \time 18/16
      e8[ bes'] c[(-- b16 bes8]) c[ aes fis16] e4		|
      \time 19/16
      fis8[ e fis16 e] bes'8[-- c8.]-- fis,2--		|
      \time 21/16
      c16[ a' bes8] a16[ bes fis'8] des8[( ees16] bes8[) g a] c,4	|
      \time 20/8
      a8[-- a'16( bes g8])-. c,8[-- a]-- a'16[( bes g8] c,8[) fis g] bes8[( a16)-. fis8 c] e[ ees8.]-- c2--	|
    
      \mark \default
    
      \time 17/16
      fis8[ e] bes'[(-- c16 aes8]) fis[ e] bes'4		|
      fis8[ e] bes'16[( b8) bes16 c] fis,2		|
      \time 21/16
      c16[ a' bes8] a16[ bes fis'8] des8[( ees16] bes8[) g a] c,4	|
      \time 22/8
      des8[ e a] g[(-- ees16 c8]) ees[ g fis16 bes,8 c] fis[ e g ees16(] c8[) e g ees8.]-- c2-- |
    
      \mark \default
    
      \time 23/16
      cis8[ e] a16[ bes e,8] f[ aes cis16 d] b8[ e] b'16[ e d a8] |
      \time 20/8
      c8[-- g16 ees des bes aes] f8[-- d]-- ais'16[(-- e8) cis] fis[ ees16] a8[(-- d,]) c[ e]-- ees[-- fis8.]-- c2-- |
      \time 6/8
      cis16[ dis fis gis] b[ g'] c8[ bes f]
      \time 11/8
      b,16[ ais' ais] gis8[ eis] ais16[ gis ais gis] dis[ eis dis eis b] cis[ a e fis cis8] |
      \time 35/16
      b'16[ gis c8-- c--] b[(-- gis16 g8]) b[-- b]-- bes[(-- aes16 g8]) a[(-- aes16 g8]) aes4(->\> g4.) |
    
      \mark \default
    
      \time 17/16
      fis8[\p e] bes'[(-- c16 aes8]) fis[ e] bes'4		|
      fis8[ e] bes'16[( b8) bes16 c] fis,2		|
      \time 18/16
      e8[ bes'] c[(-- b16 bes8]) c[ aes fis16] e4		|
      \time 19/16
      fis8[ e fis16 e] bes'8[-- c8.]-- fis,2--		|
      \time 21/16
      c16[\ff a' bes8] a16[ bes fis'8] des8[( ees16] bes8[) g a] c,4	|
      \time 20/8
      a8[-- a'16( bes g8])-. c,8[-- a]-- a'16[( bes g8] c,8[) fis g] bes8[( a16)-. fis8 c] e[ ees8.]-- c2--	|
    
      \mark \default
    
      \time 17/16
      fis8[ e] bes'[(-- c16 aes8]) fis[ e] bes'4		|
      fis8[ e] bes'16[( b8) bes16 c] fis,2 |
      \time 10/8
      c16[ a' bes8] a16[ bes fis'8] des[( ees16] bes8[) g] a16[g ees' a,8] |
      \time 9/16
      c[ des16 fis8] bes,8[-- c]-- |
      \time 4/4
      des16[ g a g] e8[( g16 ees8])-. g16[ a des,8-- ees8.]-- |
      \time 8/4
      a,16[ ees' fis c des8] aes16[ b d e] g,8[ bes16 e,8] f16[ aes cis d] f,[ bes aes] des,[ ees fis a] d,8[ f16] des[ fis g] |
      \time 13/16
      e[ g ees8.] c2-- |
      \time 9/4
      dis'16[ ais cis gis] d[ b a fis] e8[ \tempo "Pressez un peu" fis16 c' ees] g,[ des' g, b d] aes[ f' b, f'] b,[ g' b, b'] bis,8[ \tempo "Pressez encore" fis'16 gis g] cis,[ g' a8 gis16] |
      \time 11/8
      d8[ a'16]( b8[)-. bes8.]-- ees,4->_\markup { \italic "cresc. molto" }\< e-> f4.-> |
    
      \mark \default
    
      \time 6/4
      \tempo "Au mouvement"
      d'8.(^\markup { \italic "(lointain)" }\pp_\markup { \italic "(legato)" } a4 ~ a16 fis'2) cis4( ~ cis16 gis8. |
      \time 21/16
      ais4 cis8. dis4) ~ dis8. b( fis4) |
      \time 19/16
      c'8[( g e8.]) f4 ~ f16 a8.[( bes8 d]) |
      a16[( fis' cis8.) gis8]( ais cis16 dis8] b[ fis8.) c'16( g] |
      \time 13/16
      e8[ f16 a bes] d8.) a16[( fis' cis gis8]) |
      ais[ cis16( dis b] fis8.) c'16[( g e f8]) |
      a16[( bes d a fis'] cis8.) gis16[( ais cis dis b] |
    
      \mark \default
    
      \time 6/4
      fis8.) c'4( ~ c16 g2) e4( ~ e16 f8. |
      \time 21/16
      a4 bes8. d4) ~ d8. a( fis'4) |
      \time 19/16
      cis8[( gis ais8.]) cis4 ~ cis16 dis8.[( b8 fis]) |
      c'16[( g e8.) f8]( a[ bes16 d8] a8[ fis'8.) cis16( gis] |
      \time 13/16
      ais8[ cis16 dis b fis8.]) c'16[( g e f8]) |
      a8[ bes16( d a] fis'8.) cis16[( gis ais cis8]) |
      dis16[( b fis c' g] e8.) \tempo "Pressez" f16[_\markup { \italic "(non legato, martelé)" } a bes d a] |
      \time 11/8
      g[ bes c e dis b] gis[ b cis f a e ees] bes[ c e] d[ g a] d[ e8] |
    
      \mark \default
    
      \time 5/8
      \tempo "Un peu plus vif" 8 = 200
      eis16[\ff bis dis_\markup { \italic "non legaton, martelé" } ais] e[ cis b gis] fis8 |
      dis''16[ ais cis gis] e[ d b a] fis8 |
      des''16[ c g] bes[ f] dis[ b gis] fis8 |
      g''16[ d aes] f'[ c] dis,[ cis ais] fis8 |
      \tempo "Au mouvement" 8 = 176
      \time 9/8
      e8[ bes'] c[(-- b16 bes8]) c[ aes fis16] e4
      \time 19/16
      fis8[ e fis16 e] bes'8[-- c8.]-- fis,2--
      \time 5/4
      fis,1-> r4 |
    
      \mark \default
    
      \tempo "Un peu plus vif" 8 = 200
      \time 9/16
      fis'16[ e bes' c aes] fis[ e bes'8] |
      \time 5/8
      fis16[ e bes' b bes c] fis,[ e bes'8] |
      \time 13/16
      c16[ b bes c aes fis] e[ fis e] fis[ e bes'8] |
      \time 5/4
      f,2->\fff^\markup { \italic "(bronzé, cuivré)" } cis'4-> a2->
      \time 25/16
      f2-> ~ f8 cis'4-> ~ cis16 a2-> ~ a8 |
      \time 19/16
      eis'''16[\ff bis dis ais] e[ cis b gis fis] dis'[ ais cis gis] e[ d b a fis8] |
    
      \mark \default
    
      \time 13/16
      c''16[\ff b bes c aes fis] e[ fis e] fis[ e bes'8] |
      \time 15/16
      f,4.->\fff^\markup { \italic "(bronzé, cuivré)" } cis'8.-> a4.-> |
      \time 5/4
      f2-> cis'4-> a2-> |
      \time 15/16
      f8[-> cis'16-> a8]-> f8[-> cis'16-> a8]-> f8[-> cis'16-> a8]-> |
      \time 25/16
      f2-> ~ f8 cis'4-> ~ cis16 a2-> ~ a8 |
    
      \mark \default
    
      \time 9/16
      fis'16[\ff e bes' c aes] fis[ e bes'8] |
      \time 7/8
      fis16[ e bes'] b[ bes c] fis,[ e bes'] b[ bes c] fis,8 |
      \time 19/16
      c16[ a' bes a bes fis'] des[ ees bes g a c,] des[ e a g ees] c8 |
      \time 15/16
      f,4.->\fff^\markup { \italic "(bronzé, cuivré)" } cis'8.-> a4.-> |
      \time 5/8
      f4-> cis'8-> a4-> |
      \time 5/4
      f2-> cis'4-> a2-> |
      \time 15/16
      f8[-> cis'16-> a8]-> f8[-> cis'16-> a8]-> f8[-> cis'16-> a8]-> |
      f4.-> cis'8. a'4.-> |
      \time 5/4
      f'2-> cis,4->\< a''2-> |
    
      \mark \default
    
      \tempo "Plus vif" 4 = 108
      \time 8/4
      c,,16[\ff a'_\markup { \italic "non legato, martelé" } bes a bes fis'] des[ ees bes g a c,] des[ e a g ees c] ees[ g fis bes, c fis] e[ g ees c] e[ g ees c] |
      \time 11/16
      des[ e a bes] f[ aes cis d b] e8 |
      \time 13/8
      b'16[ e d a c8] g16[ ees des bes aes f d] ais'[ e cis fis ees a] d,[ c e ees fis] c8 |
    
      \mark \default
    
      \time 9/16
      fis16[\ff e bes' c aes] fis[ e bes'8] |
      \time 7/16
      fis16[\f e] bes'8[(-- c16 aes8]) |
      \time 3/8
      fis16[ e fis e fis e] fis8.[-- e]-- |
      \time 5/16
      bes'8[(-- c16 aes8]) |
      \time 13/16
      fis16[ e bes' c aes] fis[ e bes'] fis[ e bes' c aes] |
      \time 10/8
      bes8[(-- c16 aes8]) bes4.--\< c8.-- aes4.-- |
      \time 19/16
      des'16[\fff c g] bes[ f] dis[ b gis fis] g'[ d aes f' c] dis,[ cis ais] fis8 |
      \time 4/4
      \acciaccatura { g''8-> } f2\trill \breathe \acciaccatura { dis,8-> } cis2\!\trill \breathe
      \acciaccatura { b'8-> } a2\!\trill \breathe \acciaccatura { g,8-> } f2\!\trill \breathe
    
      \mark \default
    
      \tempo "Pressez insensiblement"
      \time 29/16
      e16[\f fis c' ees] g,[ des' g, b d] aes[ f' b, f'] b,[ g' b, b'] bis,[ fis' gis g] cis,[-- g' a gis] d[-- a' b bes] |
      \time 17/16
      ees,[-> bes' c b f] ees'[-> bes c b f] e[-> b' cis bis fis] g'[(->\< a])-> |
      \time 5/8
      aes,[->\ff ees' f e bes] aes'[-> ees f e bes] |
      \time 3/4
      a[ cis fis g] bes,[ ees g a] c,[ e a bes] |
      des,[ g a aes d, des] c'[\< fis, e f b] f'->-. |
      \time 7/8
      bes,,[\fff e fis f b,] a[ ees' f e] bes[ fis' gis g cis,] |
      \time 10/8
      \tempo "Pressez beaucoup"
      d[ aes' bes ees,-> bes' b] e,[(->\< b') f->( c')] fis,[(-> cis') gis(-> dis')] a[(-> e') bes(-> f')] b,[(-> fis']) |
      \tempo "Un peu moins vif" 4 = 108
      \time 25/16
      g,,,[\f a\< bes c] cis[ dis e fis] g[ a bes c] cis[ dis e fis] g[ a bis c] cis[ dis e fis g] \breathe |
      \time 4/4
      \acciaccatura { a,8-> } g2\fff\trill \breathe \acciaccatura { f8-> } ees2\!\trill \breathe |
      \acciaccatura { a8-> } g2\trill \breathe \acciaccatura { f8-> } ees2\!\trill \breathe |
      \acciaccatura { a8-> } g1\pp\startTrillSpan\< ~
      g1 ~ |
      \tempo "Rall."
      g1 ~ |
      g1 \stopTrillSpan |
    
      \mark \default
    
      \tempo "Presque lent, terrible et puissant" 8 = 76
      \time 13/16
      fis4->\ffff e'-> bes,,8[->\< c'16-> aes'8]-> |
      \time 10/8
      fis'2.->\! ~ fis8[ e,-> bes]-> r |
      \time 15/16
      fis4->\ffff e'-> bes,8[-> b'' -> bes,16-> c'8]-> |
      \time 7/8
      fis,2.-> r8 |
      \time 13/16
      e4->\ffff bes,-> c8[->\< b'16-> bes'8]-> |
      \time 9/8
      c2->\! aes4-> \breathe fis,8[-> e']-> r |
      \time 3/4
      fis4->\ffff e'-> fis,,8[-> e']-> |
      \time 5/8
      bes,4-> c'4.-> |
      \time 9/8
      fis,,1-> r8 |
    
      \mark \default
    
      \time 6/4
      \tempo "1er Mouv." 8 = 176
      d''8.(\pp_\markup { \italic "(legato)" } a4 ~ a16 fis'2) cis4( ~ cis16 gis8.) |
      \time 5/8
      \tempo "Un peu plus vif" 8 = 200
      eis''16[\ff bis dis_\markup { \italic "non legato, martelé" } ais] e[ cis b gis] fis8 |
      dis''16[ ais cis gis] e[ d b a] fis8 |
      des''16[ c g bes] f[ dis b gis] fis8 |
      g''16[ d aes f'] c[ dis, cis ais] fis8 |
      \tempo "Moins vif"
      \time 4/4
      r8 e[->\ffff fis-> e-> bes'-> c]-> fis,-> r |
      fis,1->\fermata
      \bar "|."

    }
  
  }

  \layout {}
  %\midi {}

}