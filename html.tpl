<!DOCTYPE html>

<html lang="en">

	<head>
	
		<meta charset="utf-8" />
		<title>lilypond-html-live-score</title>

		<link type="text/css" href="skin/pink.flag/jplayer.pink.flag.css" rel="stylesheet" />
		
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
		
		<script type="text/javascript">
		
			currentGrob = 0;
			intervalCounterValue = 100;
			timeOffset = 0.1;

			function svgCallback() {
			
				//$("#score").each(function() { $(this)[0].setAttribute('viewBox', (intervalCounter * 10) + ' 0 3414.3307 34.1433') });

				audioTime = ($("#jplayer").jPlayer()[0].childNodes[1].currentTime + timeOffset);

				if (currentGrob < grobs.length)
				{

					while ((grobs[currentGrob].time) <= audioTime)
					{

						grobs[currentGrob].source.find("*").each(function() {
							$(this).attr("fill-opacity", "1.0");
							$(this).attr("stroke-opacity", "1.0");
						});

						currentGrob++;
					
					}

				}

			}
		
			$(document).ready(function() {
			
				grobsObjects = $('g.ly.grob');
				
				grobsObjects.click(function() {
					$("#jplayer").jPlayer("play", $(this).data("real-time"));
				});

				grobs = [];

				grobsObjects.each(function() {

					grobTime = parseFloat($(this).data("real-time"));
					sourceObject = $(this);
					grob = {time: grobTime, source: sourceObject};

					$(this).find("*").each(function() {
						$(this).attr("fill-opacity", "0.5");
						$(this).attr("stroke-opacity", "0.5");
					});

					grobs.push(grob);

				});

				grobs.sort(function(a, b) {

					return a.time - b.time;
					
				});

				// jPlayer ------------------------

				$("#jplayer").jPlayer({
					ready: function () {
						$(this).jPlayer("setMedia", {
							title: "${audioTitle}",
							//${audioAac}
							//${audioOgg}
							//${audioMp3}
						});
					},
					cssSelectorAncestor: "#jp_container_1",
					swfPath: "/js",
					supplied: "${audioFormatsSupplied}",	// m4a, oga, mp3
					useStateClassSkin: true,
					autoBlur: false,
					smoothPlayBar: true,
					keyEnabled: true,
					remainingDuration: true,
					toggleDuration: true
				});

				$("#jplayer").bind($.jPlayer.event.play, function(event) {
					
					var intervalID = window.setInterval(svgCallback, intervalCounterValue);

				});


			});
		
		</script>
		
	</head>
	
	<body>

	<div id="jplayer" class="jp-jplayer"></div>
       	<div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
	  <div class="jp-type-single">
	    <div class="jp-gui jp-interface">
	      <div class="jp-volume-controls">
	        <button class="jp-mute" role="button" tabindex="0">mute</button>
	        <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
	        <div class="jp-volume-bar">
	          <div class="jp-volume-bar-value"></div>
	        </div>
	      </div>
	      <div class="jp-controls-holder">
	        <div class="jp-controls">
	          <button class="jp-play" role="button" tabindex="0">play</button>
	          <button class="jp-stop" role="button" tabindex="0">stop</button>
	        </div>
	        <div class="jp-progress">
	          <div class="jp-seek-bar">
	            <div class="jp-play-bar"></div>
	          </div>
	        </div>
	        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
	        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
	        <div class="jp-toggles">
	          <button class="jp-repeat" role="button" tabindex="0">repeat</button>
	        </div>
	      </div>
	    </div>
 	    <div class="jp-details">
	      <div class="jp-title" aria-label="title">&nbsp;</div>
	    </div>
	    <div class="jp-no-solution">
	      <span>Update Required</span>
	      To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
	    </div>
	  </div>
	</div>

	${svg}	

	<div id="debug">
	</div>
	
	</body>
	
</html>

